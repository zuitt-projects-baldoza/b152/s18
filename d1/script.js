console.log("Hello javascript objects");

const grades = [91, 92, 93, 94, 95];
const names = ["Joy", "Natalia", "Bianca"];

const objGrades = {
	// property: value
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: "English",
	teachers: ["Carlo", "Mashiro"],
	isActive: true,
	schools: {
		city: "Manila",
		country: "Philippines"
	},
	studentNames: [
		{
			name: "Adrian",
			batch: "152",
		},
		{
			name: "Nikko",
			batch: "152",
		}
	],
	description: function(){
		return `${this.subject}: ${this.firstGrading} of students
		${this.studentNames[0].name} and ${this.studentNames[1].name}`
	}
}

/*
	syntax:

	objReference.propertyName
	objReference["propertyName"]
*/

console.log(objGrades.firstGrading);
console.log(objGrades.subject);
console.log(objGrades["isActive"]);
console.log(objGrades["teachers"]);

console.log(objGrades.description());

console.log(objGrades.schools.country);
console.log(objGrades["schools"]["city"]);

console.log(objGrades["studentNames"][1]);
console.log(objGrades.studentNames[1].name);

// add new property in an object
objGrades.semester = "first"

// delete property in an object
delete objGrades.semester;
console.log(objGrades);

// Mini Activity
const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

// function computeGrade() {
	
// 	average1 = (studentGrades[0]["Q1"] + studentGrades[0]["Q2"] + studentGrades[0]["Q3"] + studentGrades[0]["Q4"])/4;
// 	// studentGrades[0].average = parsefloat(average1.toFixed(1))
// 	console.log(average1);

// 	average2 = (studentGrades[1]["Q1"] + studentGrades[1]["Q2"] + studentGrades[1]["Q3"] + studentGrades[1]["Q4"])/4;
// 	console.log(average2);

// 	average3 = (studentGrades[2]["Q1"] + studentGrades[2]["Q2"] + studentGrades[2]["Q3"] + studentGrades[2]["Q4"])/4;
// 	console.log(average3);

// 	average4 = (studentGrades[3]["Q1"] + studentGrades[3]["Q2"] + studentGrades[3]["Q3"] + studentGrades[3]["Q4"])/4;
// 	console.log(average4);

// 	average5 = (studentGrades[4]["Q1"] + studentGrades[4]["Q2"] + studentGrades[4]["Q3"] + studentGrades[4]["Q4"])/4;
// 	console.log(average5);

// 	average = (average1 + average2 + average3 + average4 + average5)/5;
// 	console.log(average);
// }

// computeGrade();

// Solution 2 using for loop
// for (let i=0; i<studentGrades.length; i++){

// // 	let ave = (studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4)/4
// // 	console.log(ave)

// // 	studentGrades[i].average = parseFloat(ave.toFixed(1));
// // }


// // Solution 3 - using forEach method
// studentGrades.forEach( function(element){
// 	console.log(element)

// 	let ave = (element.Q1 + element.Q2 + element.Q3 + element.Q4)/4

// 	element.average = parseFloat(ave.toFixed(1))
// })}

// console.log(studentGrades)


/* Object Constructor

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	attack: function(){
		console.log('This Pokemon tackled targetPokemon')
		console.log(`targetPokemon's health is now reduced to
		targetPokemonhealth`)
	},
	faint: function(){
		console.log(`Pokemon fainted`)
	}
}
console.log(myPokemon)

function Pokemon(name, lvl, hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target){
		console.log(target)	//object of charizard

		//change the statement to "Pikachu tackled Charizard"
		console.log(`${this.name} tackled ${target.name}`)

		//chnage the statement to "Charizard's health is now reduced to #"
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
	
		// Using compound assignment operator
		// current health - attack = health

		target.health -= this.attack

		if (target.health<10) {
			target.faint();
		}
	},
	this.faint = function(){
		console.log(`Pokemon fainted`)
	}
}

let pikachu = new Pokemon("Pikachu", 5, 50)
let charizard = new Pokemon("Charizard", 8, 40)


console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));

// Mini Activity

let squirtle = new Pokemon("Squirtle", 5, 20)
let bulbasaur = new Pokemon("Bulbasaur", 8, 20)

console.log(squirtle);
console.log(bulbasaur);

console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));
console.log(squirtle.tackle(bulbasaur));










