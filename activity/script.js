let trainer = {
	name:"Ash Ketchum",
	age:10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: ["Brock","Misty"],
	talk: function(){
		console.log("Result of dot notation")
		console.log(trainer.pokemon)
		console.log("Result of talk method")
		console.log("Pikachu! I choose you!")
	},
	}
	
console.log(trainer)
trainer.talk();


let myPokemon = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	attack: function(){
		console.log('This Pokemon tackled targetPokemon')
		console.log(`targetPokemon's health is now reduced to
		targetPokemonhealth`)
	},
	faint: function(){
		console.log(`Pokemon fainted`)
	}
}

console.log(myPokemon)

function Pokemon(name, lvl, hp, attack){
	this.name = name,
	this.level = lvl,
	this.health = hp,
	this.attack = lvl,
	this.tackle = function(target){

		console.log(`${this.name} tackled ${target.name}`)


		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)

		target.health -= this.attack

		if (target.health<10) {
			target.faint();
		}
	},
	this.faint = function(){
		console.log(`${this.name}` + " fainted")
	}
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12)
let geodude = new Pokemon("Geodude", 8, 16, 8)
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100)

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);



